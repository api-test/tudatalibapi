import json
from pathlib import Path


# noinspection PyUnresolvedReferences
class ListMixin(object):
    def list(self, data=None, params=None):
        results = self._tudatalib.http_get(self.url, data=data, params=params)

        return [self._entity_type(self._tudatalib, attrs, parent=self._parent) for attrs in results]


# noinspection PyUnresolvedReferences
class GetIDMixin(object):
    def get(self, _id, **kwargs):
        url = f'{self._tudatalib.api_url}/{self._entity}/{_id}'
        attrs = self._tudatalib.http_get(url, **kwargs)

        return self._entity_type(self._tudatalib, attrs, parent=self._parent)


# noinspection PyUnresolvedReferences
class CreateMixin(object):
    def create(self, **kwargs):
        attrs = self._tudatalib.http_post(self.url, **kwargs)
        return self._entity_type(self._tudatalib, attrs, parent=self._parent)


# noinspection PyUnresolvedReferences
class DownloadMixin(object):
    def download(self, dest=None):
        url = f'{self.url}/retrieve'

        outfile = _prepare_destination(self.name, dest)
        bitstream = self._tudatalib.http_get(url, raw=True)

        print(f'Write {outfile}')
        with outfile.open('wb') as f:
            f.write(bitstream)


class DownloadListMixin(ListMixin):
    def download_all(self, dest=None):
        files = self.list()
        for f in files:
            # Assumption: f is an object that inherits DownloadMixin
            f.download(dest=dest)


class JsonListMixin(ListMixin):
    def as_json(self):
        json_list = []
        data_list = self.list()
        for data in data_list:
            if hasattr(data, 'as_dict'):
                json_list.append(data.as_dict())
            else:
                raise TypeError(f'{data!r} is missing function `as_dict`')

        return json_list

    def save_json(self, filename='metadata.json', dest=None):
        outfile = _prepare_destination(filename, dest)

        print(f"Write JSON {outfile}")
        with outfile.open('w') as f:
            json.dump(self.as_json(), f, indent=2)


class ListGetIDMixin(ListMixin, GetIDMixin):
    pass


def _prepare_destination(filename, dest):
    if dest is None:
        dest = Path.cwd()
    else:
        dest = Path(dest).resolve()

    if not dest.exists():
        raise FileNotFoundError(f'{dest} does not exist')

    return dest / filename
