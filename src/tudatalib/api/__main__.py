import getpass
from urllib import parse

import requests
from bs4 import BeautifulSoup

from tudatalib.configs import repo_url, sso_url
from tudatalib.api.objects import (
    CollectionManager,
    CommunityManager,
    ItemManager,
    Item,
    Collection,
    Community,
)


class TUDatalib:
    _mapping = {
        'item': Item,
        'community': Community,
        'collection': Collection,
    }

    def __init__(self, email=None, tuid=None, password=None, url=None):
        self._repo_url = repo_url if url is None else url
        self._rest_url = f'{repo_url}/rest'

        self._login = False

        self.session = requests.Session()
        # too early for TU-ID?
        self.session.headers.update({'Accept': 'application/json'})

        # if email is not None or tuid is not None:
        self.login(email=email, tuid=tuid, password=password)

        # endpoints
        self.collections = CollectionManager(self)
        self.communities = CommunityManager(self)
        self.items = ItemManager(self)

    def __enter__(self):
        return self

    def __exit__(self, *args):
        self.logout()

    @property
    def url(self):
        return self._repo_url

    @property
    def api_url(self):
        return self._rest_url

    @property
    def has_login(self):
        return self._login

    def _auth_email(self, email, password):
        login_data = {
            "email": email,
            "password": password
        }
        response = self.session.post(f'{self._rest_url}/login', data=login_data)

        return response.status_code == 200

    def _auth_tuid(self, tuid, password):
        session = requests.session()

        # ------------ get shibboleth-login site -----------------------
        r1 = session.get(f'{self._rest_url}/shibboleth-login', allow_redirects=False)

        if r1.status_code == 302:  # should redirect to SSO login
            # ----------- request get redirect url-----------------------
            redirect_url2 = r1.headers['Location']
            # res2 = session.get(redirect_url2)
            # ---- set entity id instead of "choose entity" in JavaScript -----
            redirect_url3 = parse.parse_qs(parse.urlparse(redirect_url2).query)['return'][0]
            params = {
                'entityID': 'https://idp.hrz.tu-darmstadt.de/idp/shibboleth'
            }
            res3 = session.get(redirect_url3, params=params)

            # -------------post login data-----------------------
            soup = BeautifulSoup(res3.text, "lxml")
            value = soup.find('input', {'name': 'csrf_token'}).get('value')
            login_data = {
                "j_username": tuid,
                "j_password": password,
                "csrf_token": value,
                "_eventId_proceed": "submit"
            }

            params = {
                'execution': 'e1s1'
            }

            res4 = session.post(f'{sso_url}/idp/profile/SAML2/Redirect/SSO',
                                data=login_data, params=params)

            # ---- if there is e1s2? ----#
            # ---------- post saml response to tudatalib -----------------------
            if res4.status_code == 200:
                soup = BeautifulSoup(res4.text, "lxml")
                action = soup.find('form', {'method': 'post'}).get('action')
                samlresponse = soup.find('input', {'name': 'SAMLResponse'}).get('value')
                relayState = soup.find('input', {'name': 'RelayState'}).get('value')
                logindata = {
                    'RelayState': relayState,
                    'SAMLResponse': samlresponse
                }
                r4 = session.post(action, data=logindata)
                login_ok = r4.status_code == 200
            else:
                login_ok = False

            return login_ok

    def login(self, email=None, tuid=None, password=None):
        self.logout()

        if (email is None) and (tuid is None):
            email, tuid = ask_username()

        if password is None:
            password = ask_password()

        if email is not None:
            login_ok = self._auth_email(email, password)
        elif tuid is not None:
            login_ok = self._auth_tuid(tuid, password)
        else:
            login_ok = False

        if not login_ok:
            self._login = False
            print('Login failed')
            return False

        status_values = self.status()
        if status_values['authenticated']:
            print(f'Hello, {status_values["fullname"]}, welcome to TUdatalib REST!')
            self._login = True
            return True
        else:
            print('User is not authenticated')
            return False

    def logout(self):
        # logs out of TUdatalib and SSO (if login was with TU-ID)
        self.session.get(self._rest_url + '/logout')
        self.session.get(sso_url + '/logout')

        self._login = False

        self.session.close()

    def status(self):
        response = self.session.get(f'{self._rest_url}/status')
        attributes = {'authenticated': False, 'fullname': ''}
        if response.status_code == 200:
            attributes = response.json()

        return attributes

    @staticmethod
    def check_availability(url=repo_url):
        r = requests.get(f'{url}/rest/test')
        is_running = (r.text.lower() == 'rest api is running.')
        if is_running:
            return True, 'REST API is running and responsive'
        else:
            return False, f'REST API not running, response code was: {r.status_code}.'

    def get_handle(self, handle_id):
        url = f'{self.api_url}/handle/tudatalib/{handle_id}'

        json = self.http_get(url)
        return self._mapping[json['type']](self, attributes=json)

    def http_get(self, url, data=None, params=None, json=None, raw=False):
        return self.http_request('GET', url, data=data, params=params, json=json, raw=raw)

    def http_post(self, url, data=None, params=None, json=None, raw=False):
        return self.http_request('POST', url, data=data, params=params, json=json, raw=raw)

    def http_put(self, url, data=None, params=None, json=None, raw=False):
        return self.http_request('PUT', url, data=data, params=params, json=json, raw=raw)

    def http_request(self, method, url, data=None, params=None, json=None, raw=False):
        response = self.session.request(method, url, data=data, params=params, json=json)
        response.raise_for_status()
        if raw:
            return response.content
        return response.json()


def ask_username():
    username = input("Enter TU-ID or E-Mail address: ")
    email, tuid = (username, None) if '@' in username else (None, username)

    return email, tuid


def ask_password():
    password = getpass.getpass('Enter password: ')
    return password
