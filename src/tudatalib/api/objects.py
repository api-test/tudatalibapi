import json
from io import StringIO
from pathlib import Path

from tudatalib.api.mixins import (
    ListGetIDMixin,
    JsonListMixin,
    DownloadListMixin,
    GetIDMixin,
    DownloadMixin,
    CreateMixin,
)


class BaseManager:
    _entity = ''

    def __init__(self, tudatalib, parent=None):
        self._tudatalib = tudatalib
        self._parent = parent
        self._url = self._make_url()

    @property
    def url(self):
        return self._url

    def __repr__(self):
        return f'{self._entity.capitalize()} endpoint'

    def _make_url(self):
        if self._parent is None:
            _url = f'{self._tudatalib.api_url}/{self._entity}'
        else:
            _url = f'{self._parent.url}/{self._entity}'

        return _url


class BaseObject:
    """
    Base class for implemented endpoint objects

    Object-specific values from GET requests, available endpoints are specified in subclasses as class attributes.
    """
    _endpoints = {}
    _repr = ''
    _id_attribute = 'uuid'
    _entity = ''

    def __init__(self, tudatalib, attributes=None, parent=None):
        """

        :param tudatalib: TUDatalib instance that handles all the http requests
        :param attributes: dictionary from the json request
        :param parent: instance that called the manager
        """
        self._parent = parent
        self._tudatalib = tudatalib
        self._attributes = {}

        if 'expand' in attributes:
            attributes.pop('expand')

        if attributes is not None:
            for key, value in attributes.items():
                self.__setattr__(key, value)

            self._attributes = attributes

        self._url = self._make_url()

        for endpoint_name, manager in self._endpoints.items():
            self.__setattr__(endpoint_name, manager(tudatalib, parent=self))

    def __repr__(self):
        if hasattr(self, self._repr):
            return f'{self.__getattribute__(self._repr)} (ID: {self.id})'
        else:
            return f'{type(self)}'

    @property
    def url(self):
        return self._url

    @property
    def id(self):
        return getattr(self, self._id_attribute, None)

    @property
    def parent(self):
        return self._parent

    def _make_url(self):
        """
        Build the URL for the object as base for endpoints
        """
        if self.id:
            # specific object with an id is called, e.g. items/123456, so url is BASE_URL/items/123456
            base_url = f'{self._tudatalib.api_url}/{self._entity}/{self.id}'
        elif self._parent is not None:
            # objects without id, e.g. metadata
            base_url = f'{self._parent.url}/{self._entity}'
        else:
            base_url = f'{self._tudatalib.api_url}/{self._entity}'

        return base_url

    def pprint(self):
        """
        Pretty-print the attributes
        """
        s = StringIO()
        s.write(f'{self.__class__.__name__} "{self}"\n')
        for element in self._attributes:
            s.write(f'  {element!r}: {getattr(self, element)}\n')

        print(s.getvalue())

    def as_dict(self):
        ret_dict = {}
        if self._attributes is not None:
            ret_dict.update(self._attributes)

        return ret_dict


class Bitstream(BaseObject, DownloadMixin):
    _repr = 'name'
    _id_attribute = 'uuid'
    _entity = 'bitstreams'

    def upload_bitstream(self, filename):
        filename = Path(filename).expanduser().resolve()

        with filename.open('rb') as f:
            attrs = self._tudatalib.http_put(f'{self.url}/data', data=f.read(), raw=True)

        return attrs


class BitstreamManager(BaseManager, GetIDMixin, DownloadListMixin):
    _entity = 'bitstreams'
    _entity_type = Bitstream

    def upload(self, location):
        print('upload', location, type(location))
        if isinstance(location, list):
            for f in location:
                self._upload_single_file(f)
        else:
            location = Path(location).expanduser().resolve()
            if location.is_dir():
                for files in location.iterdir():
                    self._upload_single_file(files)
            else:
                self._upload_single_file(location)

    def _upload_single_file(self, filename):
        print('upload_single', filename)
        filename = Path(filename).expanduser().resolve()
        if not filename.exists():
            raise FileNotFoundError(f'{filename} does not exist.')
        bitstream = self.create(filename.name)
        bitstream.upload_bitstream(filename)

        return bitstream

    def create(self, filename):
        params = {
            'name': filename
        }
        attrs = self._tudatalib.http_post(self.url, params=params)
        bitstream = self._entity_type(self._tudatalib, attrs, parent=self._parent)

        return bitstream


class Metadata(BaseObject):
    _repr = 'value'
    _entity = 'metadata'


class MetadataManager(BaseManager, JsonListMixin):
    _entity = 'metadata'
    _entity_type = Metadata

    def upload(self, metadata):
        with open(metadata, 'r') as f:
            meta = json.load(f)
            self._tudatalib.http_put(self.url, json=meta, raw=True)


class Item(BaseObject):
    _repr = 'name'
    _endpoints = {
        'files': BitstreamManager,
        'metadata': MetadataManager
    }
    _entity = 'items'


class ItemManager(BaseManager, ListGetIDMixin, CreateMixin):
    _entity = 'items'
    _entity_type = Item

    def create(self, metadata=None, **kwargs):
        if isinstance(metadata, str):
            metadata = self._metadata_from_file(metadata)
        return super().create(json={'metadata': metadata})

    @staticmethod
    def _metadata_from_file(filename):
        filename = Path(filename).expanduser().resolve()
        with filename.open('r') as f:
            metadata = json.load(f)
        return metadata


class Collection(BaseObject):
    _repr = 'name'
    _endpoints = {
        'items': ItemManager,
    }
    _entity = 'collections'


class CollectionManager(BaseManager, ListGetIDMixin):
    _entity_type = Collection
    _entity = 'collections'


class Community(BaseObject):
    _repr = 'name'
    _endpoints = {
        'collections': CollectionManager,
    }


class CommunityManager(BaseManager, ListGetIDMixin):
    _entity_type = Community
    _entity = 'communities'


# Ugly, but working workaround for the circular dependency between Community <-> CommunityManager
Community._endpoints.update({'communities': CommunityManager})
