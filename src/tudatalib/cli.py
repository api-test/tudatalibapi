import argparse
import sys
from pathlib import Path

from tudatalib import TUDatalib


def _make_parser():

    parser = argparse.ArgumentParser(
        description='Connection to TUdatalib to create and edit items',
        add_help=True,
    )

    parser.add_argument(
        'action',
        choices=['check', 'create_item', 'upload_files', 'upload_metadata'],
        help='Selection of actions\n'
             'check: Check connection and login\n'
             'create_item: Create a new item in a collection\n'
             'upload_files: Upload files to an item\n'
             'upload_metadata: Upload metadata to an item'
    )
    parser.add_argument(
        '-e',
        '--email',
        help='Email for login',
    )
    parser.add_argument(
        '-p',
        '--password',
        help='Password for login',
    )
    parser.add_argument(
        '-c',
        '--collection',
        help='Collection ID used for item creation',
    )
    parser.add_argument(
        '-m',
        '--metadata',
        help='Filename of metadata JSON file',
    )
    parser.add_argument(
        '-i',
        '--item',
        help='Item ID used for file/metadata upload',
    )
    parser.add_argument(
        '-f',
        '--files',
        help='Filenames or directories for upload to an item',
        nargs='*',
    )

    return parser


def _select_collection(tudatalib):
    collection_list = tudatalib.collections.list()
    print('Available collections:')
    for i, collection in enumerate(collection_list, start=1):
        print(f'{i}: {collection}')

    idx = None
    while True:
        try:
            idx = int(input('Which collection shall be used? Please enter its number (0 to exit): '))
        except ValueError:
            continue

        if 0 <= idx <= len(collection_list):
            break

    if idx == 0:
        sys.exit()

    return collection_list[idx-1]


def _select_item(tudatalib):
    collection_list = tudatalib.collections.list()
    collection = _select_collection(collection_list)

    item_list = collection.items.list()
    print('Available items:')
    for i, item in enumerate(item_list, start=1):
        print(f'{i}: {item}')

    idx = None
    while True:
        try:
            idx = int(input('Which item shall be used? Please enter its number (0 to exit): '))
        except ValueError:
            continue

        if 0 <= idx <= len(item_list):
            break

    if idx == 0:
        sys.exit()

    return item_list[idx - 1]


def _get_file_location(name):
    while True:
        loc = input(f'Please enter the filepath to the {name} (or 0 to exit): ')

        if loc == '0':
            break
        else:
            loc = Path(loc).expanduser().resolve()
            if loc.exists():
                break

    return loc


def _do_checks(email, password):
    is_online, msg = TUDatalib.check_availability()
    print(msg)
    if not is_online:
        sys.exit(1)

    with TUDatalib(email=email, password=password) as _:
        pass


def _create_item(tudatalib, collection, metadata):
    # retrieve collection
    if collection is not None:
        collection_id = collection
        try:
            collection = tudatalib.collections.get(collection_id)
        except Exception:
            print('Retrieving collection failed')

    # No collection found or given, ask for collection
    if collection is None:
        collection_list = tudatalib.collections.list()
        collection_idx = _select_collection(collection_list)
        if collection_idx == -1:
            sys.exit()

        collection = collection_list[collection_idx]
    print(f'Selected collection: {collection}')

    metadata_file = None
    if metadata is not None:
        metadata_file = Path(metadata).expanduser().resolve()
        if not metadata_file.exists():
            metadata_file = None

    if metadata_file is None:
        print('No metadata file given')
        metadata_file = _get_file_location('metadata JSON file')
        if metadata_file == '0':
            sys.exit()
    print(f'Used metadata file: {metadata_file}')

    try:
        item = collection.items.create(metadata_file)
    except Exception as e:
        print(f'Creation of item failed with exception {e.args}')
        sys.exit()

    if item is not None:
        print('Item was successfully created')
        print(f'Item: {item}')

    return item


def _upload_files(tudatalib, files, item_id):
    if item_id is not None:
        item = tudatalib.items.get(item_id)
    else:
        item = _select_item(tudatalib)

    if files is None:
        files = _get_file_location('file/directory to be uploaded')

    item.files.upload(files)


def _upload_metadata(tudatalib, metadata, item_id):
    if item_id is not None:
        item = tudatalib.items.get(item_id)
    else:
        item = _select_item(tudatalib)

    if metadata is None:
        metadata = _get_file_location('metadata JSON file')

    item.metadata.upload(metadata)


def main():
    parser = _make_parser()
    args = parser.parse_args()

    action = args.action

    email, password = args.email, args.password

    if action == 'check':
        _do_checks(email, password)
    else:

        with TUDatalib(email=email, password=password) as tudatalib:
            if action == 'create_item':
                _create_item(tudatalib, args.collection, args.metadata)
            elif action == 'upload_files':
                _upload_files(tudatalib, args.files, args.item)
            elif action == 'upload_metadata':
                _upload_metadata(tudatalib, args.metadata, args.item)


if __name__ == '__main__':
    main()
